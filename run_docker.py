import subprocess
import os

# Define variables
DOCKER_IMAGE_NAME = "kernel_build_env"
KERNEL_MODULE_PATH = "./testmodule"  # Path to our module
ROOTFS_CPIO_PATH = "./rootfscpio"  # Path to our rootfs cpio folder.

def run_command(command):
    """
    Runs a shell command and appends its output to a log file.
    """
    with open("script_output.log", "a") as log_file:
        process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True, universal_newlines=True)
        for line in process.stdout:
            print(line, end='')  # Print to stdout in real-time
            log_file.write(line)  # Append output to log file

if __name__ == "__main__":
    # Remove exising volumes from the previous time to make sure the docker fetch updated folders.
    run_command("docker volume rm testmodule")
    run_command("docker volume rm rootfscpio")

    # Delete the previous Docker image. Uncomment if needed.
    # print(f"Deleting previous Docker image: {DOCKER_IMAGE_NAME}...")
    # run_command(f"docker rmi -f {DOCKER_IMAGE_NAME}")

    # Build the Docker image
    print("Building Docker image...")
    run_command(f"docker build -t {DOCKER_IMAGE_NAME} .")

    # Run the Docker container to compile the module and run qemu
    print("Running Docker container...")
    docker_run_command = (
        f"docker run --rm -it "
        f"-v {KERNEL_MODULE_PATH}:/testmodule "
        f"-v {ROOTFS_CPIO_PATH}:/rootfscpio "
        f"{DOCKER_IMAGE_NAME} "
        "/bin/bash -c './compile_and_run_qemu.sh'"
    )
    run_command(docker_run_command)

