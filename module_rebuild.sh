#!/bin/bash
# Compile module and run qemu without Docker on the Linux host OS

set -e # Exit immediately if a command exits with a non-zero status
set -o pipefail  # Ensure the script exits if any command in a pipeline fails

# Define variables
ARCH="arm"
CROSS_COMPILE="arm-linux-gnueabi-"

MODULE_NAME="testmodule"
KERNELPATH="./linux"
KERNEL_MODULE_PATH="./$MODULE_NAME"  # Path to our module
ROOTFS_CPIO_PATH="./rootfscpio"    # Path to our rootfs cpio folder


echo "Log file start new..." | tee -a script_output.log

echo "Module cross-compile make" 2>&1 | tee -a script_output.log
# Navigate to the kernel module directory
cd $KERNEL_MODULE_PATH
# Clean
make KDIR=$(pwd)/../$KERNELPATH clean
# Compile
make -j$(nproc) KDIR=$(pwd)/../$KERNELPATH ARCH=$ARCH CROSS_COMPILE=$CROSS_COMPILE
cd ..

echo "Copying new ko file in rootfs" 2>&1 | tee -a script_output.log
cp $KERNEL_MODULE_PATH/test.ko $ROOTFS_CPIO_PATH/lib/modules/test.ko

echo "Copying testing shell script into rootfs" 2>&1 | tee -a script_output.log
cp test_module.sh $ROOTFS_CPIO_PATH/root/test_module.sh

echo "Making rootfs.cpio" 2>&1 | tee -a script_output.log
cd $ROOTFS_CPIO_PATH
find . | cpio -o -H newc > ../rootfsYM.cpio
cd ..

echo "Copying zImage" 2>&1 | tee -a script_output.log
cp $KERNELPATH/arch/arm/boot/zImage zImageYM

echo "Running qemu" 2>&1 | tee -a script_output.log
qemu-system-arm -machine virt -kernel zImageYM -initrd rootfsYM.cpio -nographic -m 512 --append "root=/dev/ram0 rw console=ttyAMA0,38400 console=ttyS0 mem=512M loglevel=9"

