#!/bin/bash
# Integrate Docker into the build and run process

set -e
set -o pipefail  # Ensure the script exits if any command in a pipeline fails

# Define variables
DOCKER_IMAGE_NAME="kernel_build_env"
MODULE_NAME="testmodule"
KERNELPATH="./linux"
KERNEL_MODULE_PATH="./$MODULE_NAME"  # Path to our module
ROOTFS_CPIO_PATH="./rootfscpio" # Path to our rootfs cpio folder.

# Remove exising volumes from the previous time to make sure the docker fetch updated folders.
echo "Deleating Docker volumes..." | tee script_output.log
docker volume rm testmodule 2>&1 | tee -a script_output.log
docker volume rm rootfscpio 2>&1 | tee -a script_output.log

# Build the Docker image
echo "Building Docker image..." | tee script_output.log
docker build -t $DOCKER_IMAGE_NAME . 2>&1 | tee -a script_output.log

# Run the Docker container to compile the module and run qemu
echo "Running Docker container..." 2>&1 | tee -a script_output.log
docker run --rm -it \
    -v $KERNEL_MODULE_PATH:/testmodule \
    -v $ROOTFS_CPIO_PATH:/rootfscpio \
    $DOCKER_IMAGE_NAME \
    /bin/bash -c "./compile_and_run_qemu.sh" 2>&1 | tee -a script_output.log


