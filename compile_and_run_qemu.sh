#!/bin/bash
# Compile module and run qemu inside Docker

set -e # Exit immediately if a command exits with a non-zero status
set -o pipefail  # Ensure the script exits if any command in a pipeline fails

# Define variables
MODULE_NAME="testmodule"
KERNELPATH="./linux"
KERNEL_MODULE_PATH="./$MODULE_NAME"  # Path to our module
ROOTFS_CPIO_PATH="./rootfscpio"    # Path to our rootfs cpio folder
ARCH="arm"
CROSS_COMPILE="arm-linux-gnueabi-"
ARCH_CONFIG="multi_v7_defconfig"

# # Assuming /linux and /rootfscpio are the paths inside the container
# KERNELPATH=/linux
# ROOTFSPATH=/rootfscpio

echo "Log file start new..." | tee -a script_output.log


if [ -d "./linux/.git" ]; then
    echo "Updating existing linux kernel sources" 2>&1 | tee -a script_output.log
    git -C "./linux" pull 2>&1 | tee -a script_output.log
elif [ ! -d "./linux" ]; then
    echo "Cloning linux kernel sources" 2>&1 | tee script_output.log
    git clone --depth 1 https://github.com/torvalds/linux.git ./linux 2>&1 | tee -a script_output.log
else
    echo "./linux exists but is not a git repository. Replace with a new one" 2>&1 | tee -a script_output.log
    rm -rf "./linux"  # Forcefully remove the existing directory
    echo "Cloning linux kernel sources" 2>&1 | tee -a script_output.log
    git clone --depth 1 https://github.com/torvalds/linux.git ./linux 2>&1 | tee -a script_output.log
fi

echo "Go to the Linux Kernel source folder" 2>&1 | tee -a script_output.log
cd $KERNELPATH 

echo "Kernel cross-compile make clean" 2>&1 | tee -a script_output.log
make -j$(nproc) ARCH=$ARCH CROSS_COMPILE=$CROSS_COMPILE mrproper 2>&1 | tee -a script_output.log

echo "Create a default config for our architecture" 2>&1 | tee -a script_output.log
make ARCH=$ARCH CROSS_COMPILE=$CROSS_COMPILE $ARCH_CONFIG 2>&1 | tee -a script_output.log

echo "Kernel cross-compile make" 2>&1 | tee -a script_output.log
make -j$(nproc) ARCH=$ARCH CROSS_COMPILE=$CROSS_COMPILE 2>&1 | tee -a script_output.log
cd ..

echo "Module cross-compile make" 2>&1 | tee -a script_output.log
# Navigate to the kernel module directory
cd $KERNEL_MODULE_PATH
# Clean
make KDIR=$(pwd)/../$KERNELPATH clean
# Compile
make -j$(nproc) KDIR=$(pwd)/../$KERNELPATH ARCH=$ARCH CROSS_COMPILE=$CROSS_COMPILE
cd ..

echo "Copying new ko file in rootfs" 2>&1 | tee -a script_output.log
cp $KERNEL_MODULE_PATH/test.ko $ROOTFS_CPIO_PATH/lib/modules/test.ko

echo "Making rootfs.cpio" 2>&1 | tee -a script_output.log
cd $ROOTFS_CPIO_PATH
find . | cpio -o -H newc > ../rootfsYM.cpio
cd ..

echo "Copying zImage" 2>&1 | tee -a script_output.log
cp $KERNELPATH/arch/arm/boot/zImage zImageYM

echo "Running qemu" 2>&1 | tee -a script_output.log
qemu-system-arm -machine virt -kernel zImageYM -initrd rootfsYM.cpio -nographic -m 512 --append "root=/dev/ram0 rw console=ttyAMA0,38400 console=ttyS0 mem=512M loglevel=9"


# //====================================================================

# # Check if .config exists, if not, generate default config
# if [ ! -f "$KERNELPATH/.config" ]; then
#     echo "Generating default kernel configuration" 2>&1 | tee -a script_output.log
#     make -C $KERNELPATH ARCH=arm CROSS_COMPILE=arm-linux-gnueabi- defconfig 2>&1 | tee -a script_output.log 
# fi

# echo "Kernel cross-compile make clean" 2>&1 | tee -a script_output.log
# make -C $KERNELPATH -j4 ARCH=arm CROSS_COMPILE=arm-linux-gnueabi- clean 2>&1 | tee -a script_output.log

# echo "Kernel cross-compile make" 2>&1 | tee -a script_output.log
# make -C $KERNELPATH -j4 ARCH=arm CROSS_COMPILE=arm-linux-gnueabi- drivers/testmodule/test.ko 2>&1 | tee -a script_output.log

# echo "Copying new ko file in rootfs" 2>&1 | tee -a script_output.log
# cp $KERNELPATH/drivers/testmodule/test.ko $ROOTFSPATH/lib/modules/test.ko 2>&1 | tee -a script_output.log

# echo "Making rootfs.cpio" 2>&1 | tee -a script_output.log
# cd $ROOTFSPATH
# find . | cpio -o -H newc > /rootfsYM.cpio 2>&1 | tee -a script_output.log
# cd ..

# echo "Copying zImage" 2>&1 | tee -a script_output.log
# cp $KERNELPATH/arch/arm/boot/zImage /zImageYM 2>&1 | tee -a script_output.log

# echo "Running qemu" 2>&1 | tee -a script_output.log
# qemu-system-arm -machine virt -kernel /zImageYM -initrd /rootfsYM.cpio -nographic -m 512 --append "root=/dev/ram0 rw console=ttyAMA0,38400 console=ttyS0 mem=512M loglevel=9" 2>&1 | tee -a script_output.log

