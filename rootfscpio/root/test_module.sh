#!/bin/bash

# Define the path to your kernel module
MODULE_PATH="/lib/modules/test.ko"

# Insert the module
echo "Inserting the module..."
# insmod /lib/modules/test.ko
insmod $MODULE_PATH

# Check if the module is loaded
echo "Checking if the module is loaded..."
if lsmod | grep -q "test"; then
    echo "Module is loaded successfully."
else
    echo "Failed to load the module."
    exit 1
fi

# Optionally, interact with your module here
# For example, if your module writes to /dev or produces output in dmesg
echo "Checking kernel messages..."
dmesg | tail

# Wait for a bit to interact or test further
echo "Waiting for 10 seconds for any manual checks..."
sleep 10

# Remove the module
echo "Removing the module..."
rmmod test

# Check dmesg for any messages on removal or errors
echo "Checking kernel messages after removal..."
dmesg | tail

echo "Module testing completed."