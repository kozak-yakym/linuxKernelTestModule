#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/interrupt.h>
#include <linux/sched.h>
#include <linux/slab.h>
#include <linux/workqueue.h>
#include <linux/debugfs.h>
#include <linux/spinlock.h>
#include <linux/time.h>
#include <linux/circ_buf.h>

// TODO:
// - Format by Linux Kernel code convention
// - Remove magic numbers
// - Remove(or move under ifdefs) temporary commented code.

#define DEBUG_INFO_EN 0 /* [0..1] 1 - Enables some extra debug in this module */

#define CMD_START "start"
#define CMD_STOP "stop"
#define CMD_CLEAR "clear"

/* modinfo information */
MODULE_LICENSE("GPL");
MODULE_AUTHOR("kozak-yakym <kozak-yakym@users.noreply.github.com>");
MODULE_DESCRIPTION("kozak-yakym ringbuf module");

#define UART_PL011_SHARED_IRQ 53 /* In our case  53: 18 GIC-0 33 Level uart-pl011 */
#define DBG_BLEN 256             /* count_dbg_buf length */
#define DBG_CIRC_BUF_SIZE 1024

/* IRQ params */
static int irq = UART_PL011_SHARED_IRQ, my_dev_id, irq_counter = 0;
/* init module params */
module_param(irq, int, S_IRUGO);

/* tasklet pointer */
static struct tasklet_struct *tst_tasklet_s;
/* tasklet input variable */
unsigned long int taskletVar = 0;

/* debugfs dir and files struct */
static struct dentry *debug_dir;
static struct dentry *debug_dentry_count;
static struct dentry *debug_dentry_tmstmp;
static spinlock_t my_lock;

/* buffer for count debug messages */
static char count_dbg_buf[DBG_BLEN];
/* buffer for writing from userspace */
char *temp_buff;
/* circular buffer struct */
struct circ_buf *debug_circ_b;
/* circular buff writing flag */
char cb_w_fl = 0;

/* Circular Buffer Functions */
static struct circ_buf *dbg_circ_buf_alloc(void);
static void circ_buf_free(struct circ_buf *cb);
static void circ_buf_clear(struct circ_buf *cb);
static int circ_buf_data_avail(struct circ_buf *cb);
static int circ_buf_space_avail(struct circ_buf *cb);
static int circ_buf_put(struct circ_buf *cb, const char *buf, int count) __attribute__((unused));
static int circ_buf_get(struct circ_buf *cb, char *buf, int count) __attribute__((unused));
/* Allocate a circular buffer and all associated memory. */
static struct circ_buf *dbg_circ_buf_alloc(void)
{
  struct circ_buf *cb;

  cb = (struct circ_buf *)kmalloc(sizeof(struct circ_buf), GFP_KERNEL);
  if (cb == NULL)
    return NULL;

  cb->buf = kmalloc(DBG_CIRC_BUF_SIZE, GFP_KERNEL);
  if (cb->buf == NULL)
  {
    kfree(cb);
    return NULL;
  }

  circ_buf_clear(cb);

  return cb;
}

/* Free the buffer and all associated memory. */
static void circ_buf_free(struct circ_buf *cb)
{
  kfree(cb->buf);
  kfree(cb);
}

/* Clear out all data in the circular buffer. */
static void circ_buf_clear(struct circ_buf *cb)
{
  cb->head = cb->tail = 0;
}

/* Return the number of bytes of data available in the circular buffer. */
static int circ_buf_data_avail(struct circ_buf *cb)
{
  return CIRC_CNT(cb->head, cb->tail, DBG_CIRC_BUF_SIZE);
}

/* Return the number of bytes of space available in the circular buffer. */
static int circ_buf_space_avail(struct circ_buf *cb)
{
  return CIRC_SPACE(cb->head, cb->tail, DBG_CIRC_BUF_SIZE);
}

/* Copy data data from a user buffer and put it into the circular buffer.
 * Restrict to the amount of space available.
 *
 * Return the number of bytes copied.
 */
static int circ_buf_put(struct circ_buf *cb, const char *buf, int count)
{
  int c, ret = 0;

  while (1)
  {
    c = CIRC_SPACE_TO_END(cb->head, cb->tail, DBG_CIRC_BUF_SIZE);
    if (count < c)
      c = count;
    if (c <= 0)
      break;
    memcpy(cb->buf + cb->head, buf, c);
    cb->head = (cb->head + c) & (DBG_CIRC_BUF_SIZE - 1);
    buf += c;
    count -= c;
    ret += c;
  }

  return ret;
}

/* Copy data data from a user buffer and put it into the circular buffer.
 * If the buffer is full then FIFO
 * Why do we use the Circular buffer for FIFO? For getting log window like dmesg log.
 *
 * Return the number of bytes copied.
 */
static int circ_buf_put_fifo(struct circ_buf *cb, const char *buf, int count)
{
  int t_shift = 0;

  if (count <= 0)
    return 0;

  if (count > DBG_CIRC_BUF_SIZE)
    count = DBG_CIRC_BUF_SIZE;

  if (cb->head + count > DBG_CIRC_BUF_SIZE - 1)
  {
    memcpy(cb->buf + cb->head, buf, (DBG_CIRC_BUF_SIZE - cb->head));
    memcpy(cb->buf + 0, buf + (DBG_CIRC_BUF_SIZE - cb->head), count - (DBG_CIRC_BUF_SIZE - cb->head));
  }
  else if (cb->head + count <= DBG_CIRC_BUF_SIZE - 1)
  {
    memcpy(cb->buf + cb->head, buf, count);
  }

  t_shift = count - circ_buf_space_avail(cb);
  if (t_shift < 0)
    t_shift = 0;
  cb->head = (cb->head + count) & (DBG_CIRC_BUF_SIZE - 1);
  cb->tail = (cb->tail + t_shift) & (DBG_CIRC_BUF_SIZE - 1);

  return count;
}

/* Get data from the circular buffer and copy to the given buffer.
 * Restrict to the amount of data available.
 *
 * Return the number of bytes copied.
 */
static int circ_buf_get(struct circ_buf *cb, char *buf, int count)
{
  int c, ret = 0;

  while (1)
  {
    c = CIRC_CNT_TO_END(cb->head, cb->tail, DBG_CIRC_BUF_SIZE);

    if (count < c)
      c = count;
    if (c <= 0)
      break;
    memcpy(buf, cb->buf + cb->tail, c);
    cb->tail = (cb->tail + c) & (DBG_CIRC_BUF_SIZE - 1);
    buf += c;
    count -= c;
    ret += c;
  }

  return ret;
}

/* Make dump of data from the circular buffer and copy to the given buffer.
 * Restrict to the amount of data available.
 *
 * Return the number of bytes copied.
 */
static int circ_buf_get_fifo(struct circ_buf *cb, char *buf, int count, loff_t *position)
{
  static int header_pos = 0;
  int c_bsize = circ_buf_data_avail(cb);
  int c_toend = CIRC_CNT_TO_END(cb->head, cb->tail, DBG_CIRC_BUF_SIZE);
  int c_fstart = c_bsize - c_toend;

  if (c_bsize <= 0)
    return 0;

  if (cb->head > cb->tail)
  {
    memcpy(buf, cb->buf + cb->tail, c_bsize);
  }
  else if (cb->head < cb->tail)
  {
    memcpy(buf, cb->buf + cb->tail, c_toend);
    memcpy(buf + c_toend, cb->buf + 0, c_fstart);
  }

  if (header_pos == cb->head)
  {
    return 0;
  }
  else
  {
    header_pos = cb->head;
    return c_bsize;
  }
}

/* count read file operation */
static ssize_t count_reader(struct file *fp, char __user *user_buffer,
                            size_t count, loff_t *position)
{
  ssize_t ret;
  unsigned long flags;

  /* Lock with disabling of interruptions */
  spin_lock_irqsave(&my_lock, flags);
  ret = simple_read_from_buffer(user_buffer, count, position, count_dbg_buf, DBG_BLEN);
  /* Free with restoring of interruptions */
  spin_unlock_irqrestore(&my_lock, flags);

  return ret;
}

/* count write file operation */
static ssize_t count_writer(struct file *fp, const char __user *user_buffer,
                            size_t count, loff_t *position)
{
  return 0;
}

/* time log read file operation */
static ssize_t time_reader(struct file *fp, char __user *user_buffer,
                           size_t count, loff_t *position)
{
  ssize_t ret;
  unsigned long flags;

  /* Lock with disabling of interruptions */
  spin_lock_irqsave(&my_lock, flags);
  ret = circ_buf_get_fifo(debug_circ_b, temp_buff, circ_buf_data_avail(debug_circ_b), position);
  ret = simple_read_from_buffer(user_buffer, count, position, temp_buff, circ_buf_data_avail(debug_circ_b));
  /* Free with restoring of interruptions */
  spin_unlock_irqrestore(&my_lock, flags);

  return ret;
}

/* time log write file operation */
static ssize_t time_writer(struct file *fp, const char __user *user_buffer,
                           size_t count, loff_t *position)
{
  ssize_t ret;
  unsigned long flags;

  if (count > DBG_BLEN)
    return -EINVAL;
  /* Lock with disabling of interruptions */
  spin_lock_irqsave(&my_lock, flags);

  ret = simple_write_to_buffer(temp_buff, DBG_CIRC_BUF_SIZE, position, user_buffer, count);
  if (!memcmp(CMD_START, temp_buff, strlen(CMD_START)))
  {
    printk(KERN_INFO "Start writing\n"); /* start recording to circular buffer */
    cb_w_fl = 1;
  }

  if (!memcmp(CMD_STOP, temp_buff, strlen(CMD_STOP)))
  {
    printk(KERN_INFO "Stop writing\n"); /* stop recording to circular buffer */
    cb_w_fl = 0;
  }

  if (!memcmp(CMD_CLEAR, temp_buff, strlen(CMD_CLEAR)))
  {
    printk(KERN_INFO "Circ buf clearing\n"); /* clearing circular buffer */
    circ_buf_clear(debug_circ_b);
  }

  /* Free with restoring of interruptions */
  spin_unlock_irqrestore(&my_lock, flags);

  return ret;
}

/* debugfs structures  */
static const struct file_operations fops_debug_count = {
    .read = count_reader,
    .write = count_writer,
};
static const struct file_operations fops_debug_time = {
    .read = time_reader,
    .write = time_writer,
};

/* uart interrupt */
static irqreturn_t uart_interrupt_top(int irq, void *dev_id)
{
  struct timespec64 now;
  char b_tmp[100] = {0};
  int msg_len = 0;

  irq_counter++;
#if DEBUG_INFO_EN == 1
  printk(KERN_INFO "In the ISR: counter = %d\n", irq_counter);
  printk(KERN_INFO "pl011 interruption. Interrupted pid: %d\n", current->pid);
#endif /* DEBUG_INFO_EN == 1 */

  /* lock */
  spin_lock(&my_lock);
  if (cb_w_fl)
  {
    /* Set value to the buffer */
    sprintf(count_dbg_buf, "IRQ Counter is: %d\n", irq_counter);

    ktime_get_real_ts64(&now);
    msg_len = sprintf(b_tmp, "[%02lld-%02lld-%03ld] %d\n", ((now.tv_sec) / 60) % 60, (now.tv_sec) % 60, now.tv_nsec / 1000000, irq_counter);
#if DEBUG_INFO_EN == 1
    sprintf(b_tmp, "%d %02d-%02d-%02d\n", irq_counter, ((now.tv_sec) / 60) % 60, (now.tv_sec) % 60, now.tv_usec / 1000);
    printk(KERN_INFO "%s\n", b_tmp);
#endif /* DEBUG_INFO_EN == 1 */

    circ_buf_put_fifo(debug_circ_b, b_tmp, msg_len);
    /* We can do here: circ_buf_put(debug_circ_b, b_tmp, msg_len); */
  }
  /* unlock */
  spin_unlock(&my_lock);

  // FIXME: Investigate the purpose of the:   tasklet_schedule(tst_tasklet_s);

  // FIXME: Investigate the purpose of the:   return IRQ_NONE;  /* we return IRQ_NONE because we are just observing */
  return IRQ_WAKE_THREAD;
}

/* uart interrupt */
static irqreturn_t uart_interrupt_bottom(int irq, void *dev_id)
{
#if DEBUG_INFO_EN == 1
  printk(KERN_INFO "pl011 bottom interruption. Interrupted pid: %d\n", current->pid);
#endif /* DEBUG_INFO_EN == 1 */
  return IRQ_HANDLED;
}

static void my_tasklet_function(unsigned long data)
{
  printk("call tasklet function\n");
  printk("tasklet data: %d\n", *(char *)data);
  printk(KERN_INFO "Tasklet function. Interrupted pid: %d\n", current->pid);
}

// FIXME: Investigate the purpose of the: DECLARE_TASKLET (tst_tasklet_s, my_tasklet_function, (unsigned long) &tmpVar);

static int __init hello_init(void)
{
  spin_lock_init(&my_lock);

  temp_buff = kmalloc(DBG_CIRC_BUF_SIZE, GFP_KERNEL);

  /* circular buffer allocate */
  debug_circ_b = dbg_circ_buf_alloc();
  if (debug_circ_b == NULL)
  {
    printk(KERN_ALERT "Error: %s - out of memory\n", __FUNCTION__);
    circ_buf_free(debug_circ_b);
    return -ENOMEM;
  }
  circ_buf_clear(debug_circ_b);
  printk(KERN_INFO "Circular buffer inited\n");

  /* create a directory by the name uart-pl011 in /sys/kernel/debugfs */
  debug_dir = debugfs_create_dir("uart-pl011", NULL);
  if (debug_dir == NULL)
  {
    // FIXME: Investigate the purpose of the:    debugfs_remove(debug_dir);
    printk(KERN_ALERT "Error: Could not initialize /sys/kenel/debug/uart-pl011 dir\n");
    return -ENOMEM;
  }
  /** create a files in the above directory
   * This requires read and write file operations */
  debug_dentry_count = debugfs_create_file("uart-pl011-count", 0644, debug_dir, NULL, &fops_debug_count);
  debug_dentry_tmstmp = debugfs_create_file("uart-pl011-timestamp", 0644, debug_dir, NULL, &fops_debug_time);
  if (debug_dentry_count == NULL || debug_dentry_tmstmp == NULL)
  {
    printk(KERN_ALERT "Error: Could not initialize /sys/kernel/debug/uart-pl011/uart-pl011 file\n");
    return -ENOMEM;
  }
  printk(KERN_INFO "Debug files created\n");

  /* memory alloc for tasklet */
  tst_tasklet_s = kmalloc(sizeof(struct tasklet_struct), GFP_KERNEL);
  if (!tst_tasklet_s)
  {
    pr_err("Failed to allocate memory for tasklet\n");
    return -ENOMEM;
  }

  /* tasklet init function
   * tasklet name
   * tasklet callback
   * variable */
  tasklet_init(tst_tasklet_s, my_tasklet_function, taskletVar);

  printk(KERN_INFO "Tasklet inited\n");

  // FIXME: Investigate the purpose of the:  if ( request_irq( irq, uart_interrupt_top, IRQF_SHARED, "uart_interrupt", &my_dev_id ) )
  if (request_threaded_irq(irq, uart_interrupt_top, uart_interrupt_bottom, IRQF_SHARED, "uart_interrupt", &my_dev_id))
  {
    printk(KERN_ALERT "-= something is wrong here =-\n");
    return -1;
  }
  printk(KERN_INFO "Successfully loading ISR handler on IRQ %d\n", irq);

  printk("Hello world\n");
  return 0;
}

static void __exit hello_exit(void)
{
  circ_buf_free(debug_circ_b);
  synchronize_irq(irq);
  free_irq(irq, &my_dev_id);
  printk(KERN_INFO "Successfully unloading, irq_counter = %d\n", irq_counter);

  /** removing the directory recursively which
   * in turn cleans all the file */
  debugfs_remove_recursive(debug_dir);

  // FIXME: Investigate the purpose of the:  debugfs_remove(debug_dentry_count);
  // FIXME: Investigate the purpose of the:  debugfs_remove(debug_dir);
  printk(KERN_INFO "Debugfs files removed\n");

  printk("Goodbye, world\n");
}

module_init(hello_init);
module_exit(hello_exit);

//------------------------------------------------------
/* info section */
/*
struct file {
  mode_t f_mode;
  loff_t f_pos;
  unsigned short f_flags;
  unsigned short f_count;
  unsigned long f_reada, f_ramax, f_raend, f_ralen, f_rawin;
  struct file *f_next, *f_prev;
  int f_owner;         // pid or -pgrp where SIGIO should be sent
  struct inode * f_inode;
  struct file_operations * f_op;
  unsigned long f_version;
  void *private_data;  // needed for tty driver, and maybe others
};
*/
