#include <linux/kernel.h> /* Here is printk() e.t.c. */
#include <linux/module.h> /* For modules */
#include <linux/init.h> /* Macroses */
#include <linux/fs.h>
#include <linux/interrupt.h> /* Interrupts */
#include <asm/uaccess.h> /* put_user */

// Modinfo information
MODULE_LICENSE( "GPL" );
MODULE_AUTHOR( "Yashka <kozak-yakym@yandex.ua>" );
MODULE_DESCRIPTION( "My nice module" );
MODULE_SUPPORTED_DEVICE( "test" ); /* /dev/testdevice */

#define SUCCESS 0
#define DEVICE_NAME "test" /* Device name */

// The other operation for module existing
static int device_open( struct inode *, struct file * );
static int device_release( struct inode *, struct file * );
static ssize_t device_read( struct file *, char *, size_t, loff_t * );
static ssize_t device_write( struct file *, const char *, size_t, loff_t * );

// Static. For global in module
static int major_number; /* Major number of our driver */
static int is_device_open = 0; /* Do we use the device? */
static char text[ 5 ] = "test\n"; /* String to output when  */
static char* text_ptr = text; /* text pointer */

// device operation handler names
static struct file_operations fops =
 {
  .read = device_read,
  .write = device_write,
  .open = device_open,
  .release = device_release
 };


#define TEST_SHARED_IRQ 53 // In our case  53: 18 GIC-0 33 Level uart-pl011     
	
static int irq = TEST_SHARED_IRQ, my_dev_id, irq_counter = 0;
module_param( irq, int, S_IRUGO );

static irqreturn_t my_interrupt( int irq, void *dev_id ) {                   
   irq_counter++;
   printk( KERN_INFO "In the ISR: counter = %d\n", irq_counter );
   return IRQ_NONE;  /* we return IRQ_NONE because we are just observing */
}           



// Loading module function.
static int __init test_init( void )
{
 if ( request_irq( irq, my_interrupt, IRQF_SHARED, "my_interrupt", &my_dev_id ) )
 	return -1;                  
 printk( KERN_INFO "Successfully loading ISR handler on IRQ %d\n", irq );


 printk( KERN_ALERT "TEST driver loaded!\n" );

 // Registering device
 major_number = register_chrdev( 0, DEVICE_NAME, &fops );

 if ( major_number < 0 )
 {
  printk( "Registering the character device failed with %d\n", major_number );
  return major_number;
 }

 printk( "Test module is loaded!\n" );
	
 // Major number printing
 printk( "Please, create a dev file with 'mknod /dev/test c %d 0'.\n", major_number );

 return SUCCESS;
}

// Module unloading function
static void __exit test_exit( void )
{
 synchronize_irq( irq );        
 free_irq( irq, &my_dev_id );            
 printk( KERN_INFO "Successfully unloading, irq_counter = %d\n", irq_counter );
 // Release device
 unregister_chrdev( major_number, DEVICE_NAME );

 printk( KERN_ALERT "Test module is unloaded!\n" );
}

// Load-unload functions
module_init( test_init );
module_exit( test_exit );

static int device_open( struct inode *inode, struct file *file )
{
 text_ptr = text;

 if ( is_device_open )
  return -EBUSY;

 is_device_open++;

 return SUCCESS;
}

static int device_release( struct inode *inode, struct file *file )
{
 is_device_open--;
 return SUCCESS;
}

static ssize_t

device_write( struct file *filp, const char *buff, size_t len, loff_t * off )
{
 printk( "Sorry, this operation isn't supported.\n" );
 return -EINVAL;
}

static ssize_t device_read( struct file *filp, /* include/linux/fs.h */
       char *buffer, /* buffer */
       size_t length, /* buffer length */
       loff_t * offset )
{
 int byte_read = 0;

 if ( *text_ptr == 0 )
  return 0;

 while ( length && *text_ptr )
 {
  put_user( *( text_ptr++ ), buffer++ );
  length--;
  byte_read++;
 }

 return byte_read;
}

