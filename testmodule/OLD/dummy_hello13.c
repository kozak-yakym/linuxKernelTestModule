#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/errno.h>
#include <linux/slab.h>
#include <linux/irqreturn.h>
#include <linux/interrupt.h>
#include <linux/debugfs.h>
#include <linux/spinlock.h>
#include <linux/gfp.h>
#include <asm/uaccess.h>



#define DUMMY_HELLO_IRQ 54

#define HELLO_BUFFER_SIZE (1 << 4)
#if (0 != (HELLO_BUFFER_SIZE & (HELLO_BUFFER_SIZE - 1) ) )
#error "Buffer size must be power of two."
#endif

#define HELLO_STR_BUFFER_SIZE 33

#define HELLO_ALLIGN_SIZE 16
#if (0 != (HELLO_ALLIGN_SIZE & (HELLO_ALLIGN_SIZE - 1) ) )
#error "Allignment size must be power of two."
#endif
#define HELLO_ALLIGN_MASK (HELLO_ALLIGN_SIZE - 1)



// Dummy allocator.
// Workflow:
// - allocator asks one memory page in the Kernel;
// - for module's internal usage the allocator provides pointers inside the page;
// - also the allocator alligns pointers inside the page (in some cases it can improve performance);
// - the allocator do not manage allocated memory (it is not implemented release of allocated memory);
// - the allocator free whole page when memory for internal usage is no more required.
//
//
//  |<---           PAGE_SIZE       /  / --->|
//                                  \  `
//  +------------+--+---------------/  /-----+
//  |            |  |               \  \     |
//  |used memory |  | free memory   /  /     |
//  |            |  |               \  \     |
//  +------------+--+---------------/  /-----+
//                                  \  `
//  ^               ^               /  /     ^
//  |<-  size  ->|  |                        |
//
// b_addr         f_addr              b_addr + PAGE_SIZE
//              (alligned)
//
static struct dummy_allocation_table
{
    unsigned char is_alloc; // Is memory already allocated (it means Kernel's memory).
    unsigned long b_addr;   // Base address pointer (pointer to the begin of allocated page).
    unsigned long f_addr;   // Pointer on free memory.
}   mem;


void dummy_memory_alloc (int flags)
{
    // Get one page and zeroed it.
    mem.is_alloc = 1;
    mem.b_addr   = __get_free_pages (flags, 0);
    mem.f_addr   = mem.b_addr;
    memset ( (void *)mem.b_addr, 0, PAGE_SIZE);
}


void dummy_memory_free (void)
{
    // Release allocated page.
    free_pages (mem.b_addr, 0);
    mem.is_alloc = 0;
    mem.b_addr   = 0;
    mem.f_addr   = 0;
}


void * dummy_memory_get (unsigned long size)
{
    // Return pointer to free memory if it is available.
    void * res = NULL;

    // If memory is already allocated...
    if (mem.is_alloc) {
        // and if free memory is available...
        if (mem.b_addr + PAGE_SIZE
         >= mem.f_addr + size) {
            // allocate memory...
            res = (void *)mem.f_addr;
            mem.f_addr += size;
            // and align pointer for future allocations.
            mem.f_addr += (HELLO_ALLIGN_SIZE - (mem.f_addr & HELLO_ALLIGN_MASK) ) & HELLO_ALLIGN_MASK;
        }
    }

    printk (KERN_INFO "Hello allocator: requested %lu, free memory offset %lu.\n", size, mem.f_addr - mem.b_addr);

    return res;
}


void dummy_memory_release (unsigned long addr)
{
    // Release unused memory.
    // Not required in current version.
}



// Container for module's data.
static struct hello_data
{
    struct hello_debug                      // Data related with dubugfs.
    {
        struct dentry     * dir;            // Debug FS module's root directory.
        struct dentry     * file_cnt;       // Debug FS file with counter info.
        struct dentry     * file_time;      // Debuf FS file with timestamp info.
    }                       debug;

    spinlock_t              lock;

    struct hello_counter                    // Data related with interrupts counter.
    {
        unsigned int        value;          // Current value of interruptions number.
        unsigned int        reported;       // Reported value.
        char              * view;           // Buffer for stringification of counter.
    }                       counter;

    struct hello_timestamp                  // Data related with timestamp buffer.
    {
        struct history_item
        {
            unsigned int    counter;
            struct timeval  time;
        }                 * history;        // Timestamp buffer.
        int                 history_cur;    // Pointer to current element in the timestamp buffer.
        unsigned int        reported;       // Key of reported value.
        char              * view;           // Buffer for formatting of message.
    }                       timestamp;
}                         * handler;



static ssize_t hello_counter_read (struct file * filep, char * buffer, size_t len, loff_t * offset)
{
    unsigned int res  = 0;
    unsigned int todo = 0;   // Total number of bytes to copy.
    unsigned int need = 0;   // Number of bytes not copied yet.

    // Get data using synchronization.
    spin_lock (&handler->lock);
    res = handler->counter.value;
    spin_unlock (&handler->lock);

    // If current value has not been reported yet...
    if (handler->counter.reported != res) {
        // Format data.
        need = snprintf (handler->counter.view, HELLO_STR_BUFFER_SIZE
              , "%d\n"
              , res
        );
        need = need > len ? len : need;
        todo = need;

        // Write formatted data to user space.
        while (need) {
            put_user (handler->counter.view [todo - need--], buffer++);
        }

        // Update reported value.
        handler->counter.reported = res;

        return todo - need;
    }
    else {
        return 0;
    }
}

static ssize_t hello_counter_write (struct file * filep, const char * buffer, size_t len, loff_t * offset)
{
    unsigned int res = 0;
    unsigned int val = 0;

    res = kstrtouint_from_user (buffer, len, 10, &val);
    if (res < 0) {
        return res;
    }

    // Set data using synchronization.
    spin_lock (&handler->lock);
    handler->counter.value = val;
    spin_unlock (&handler->lock);

    return len;
}

static struct file_operations counter_fops =
{
    .read  = hello_counter_read,
    .write = hello_counter_write,
};



static ssize_t hello_timestamp_read (struct file * filep, char * buffer, size_t len, loff_t * offset)
{
    struct history_item item;
    unsigned int todo = 0;    // Total number of bytes to copy.
    unsigned int need = 0;    // Number of bytes not copied yet.

    // Get data using synchronization.
    spin_lock (&handler->lock);
    // Read data from previous position (where lies current data) in buffer.
    item = * (handler->timestamp.history + ( (handler->timestamp.history_cur + HELLO_BUFFER_SIZE - 1) & (HELLO_BUFFER_SIZE - 1) ) );
    spin_unlock (&handler->lock);

    // If current value has not been reported yet...
    if (handler->timestamp.reported != item.counter) {
        // Format data.
        need = snprintf (handler->timestamp.view, HELLO_STR_BUFFER_SIZE
              , "%u %02u-%02u-%03u\n"
              , item.counter
              , (unsigned int)( (item.time.tv_sec  / 60)  % 60)
              , (unsigned int)(item.time.tv_sec           % 60)
              , (unsigned int)(item.time.tv_usec / 1000)
        );
        need = need > len ? len : need;
        todo = need;

        // Write formatted data to user space.
        while (need) {
            put_user (handler->timestamp.view [todo - need--], buffer++);
        }

        // Update reported value.
        handler->timestamp.reported = item.counter;

        return todo - need;
    }
    else {
        return 0;
    }
}

static struct file_operations timestamp_fops =
{
    .read  = hello_timestamp_read,
    .write = hello_counter_write,
};



static irqreturn_t hello_irq_debugfs_top (int irq, void *dev_id)
{
    unsigned long flags;
    // Lock with disabling of interruptions.
    spin_lock_irqsave (&handler->lock, flags);
    // Do work.
    // Update interrupts count.
    ++handler->counter.value;
    (handler->timestamp.history + handler->timestamp.history_cur)->counter = handler->counter.value;
    // Get current time.
    do_gettimeofday (&(handler->timestamp.history + handler->timestamp.history_cur)->time);
    // Update circular buffer pointer.
    handler->timestamp.history_cur = (handler->timestamp.history_cur + 1) & (HELLO_BUFFER_SIZE - 1);
    // Free with restoring of interruptions.
    spin_unlock_irqrestore (&handler->lock, flags);

    return IRQ_HANDLED;
}



static int __init hello_init (void)
{
    int ret;

    dummy_memory_alloc (GFP_KERNEL);

    handler                    = dummy_memory_get (sizeof (struct hello_data) );
    handler->counter.view      = dummy_memory_get (HELLO_STR_BUFFER_SIZE);
    handler->timestamp.history = dummy_memory_get (sizeof (struct history_item) * HELLO_BUFFER_SIZE);
    handler->timestamp.view    = dummy_memory_get (HELLO_STR_BUFFER_SIZE);
    if (!handler
     || !handler->counter.view
     || !handler->timestamp.history
     || !handler->timestamp.view) {
        ret = -ENOMEM;
       goto ret_fail_mem;
    }

    spin_lock_init (&handler->lock);
    handler->counter.value         =  0;
    handler->counter.reported      = -1;
    handler->timestamp.reported    = -1;
    handler->timestamp.history_cur =  0;

    handler->debug.dir = debugfs_create_dir ("dummy-hello", NULL);
    if (!handler) {
        ret = -ENOENT;
        goto ret_fail_crdir;
    }
    if ( (int)handler == -ENODEV) {
        ret = -ENODEV;
        goto ret_fail_crdir;
    }

    handler->debug.file_cnt  = debugfs_create_file ("uart-pl011-count",     S_IRUSR | S_IWUSR, handler->debug.dir, NULL, &counter_fops);
    handler->debug.file_time = debugfs_create_file ("uart-pl011-timestamp", S_IRUSR | S_IWUSR, handler->debug.dir, NULL, &timestamp_fops);
    if (!handler->debug.file_cnt
     || !handler->debug.file_time) {
        ret = -ENOENT;
        goto ret_fail_crfile;
    }
    if ( (int)handler->debug.file_cnt  == -ENODEV
      || (int)handler->debug.file_time == -ENODEV) {
        ret = -ENODEV;
        goto ret_fail_crfile;
    }

    ret = request_irq(DUMMY_HELLO_IRQ, hello_irq_debugfs_top, IRQF_SHARED, "dummy-hello", handler);
    if (ret) {
        ret = -ENXIO;
        goto ret_fail_reqirq;
    }


    // ret_success:
        printk (KERN_INFO "Hello inited.\n");
        return 0;

    ret_fail_mem:
        dummy_memory_free ();
        return ret;

    ret_fail_crdir:
        dummy_memory_free ();
        return ret;

    ret_fail_crfile:
        debugfs_remove_recursive (handler->debug.dir);
        dummy_memory_free ();
        return ret;

    ret_fail_reqirq:
        debugfs_remove_recursive (handler->debug.dir);
        dummy_memory_free ();
        return ret;
}



static void __exit hello_exit (void)
{
    free_irq (DUMMY_HELLO_IRQ, handler);
    debugfs_remove_recursive (handler->debug.dir);
    dummy_memory_free ();

    printk (KERN_INFO "Hello exited.\n");
}



module_init (hello_init);
module_exit (hello_exit);



MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("Dummy module");
MODULE_AUTHOR("Gluttton");

