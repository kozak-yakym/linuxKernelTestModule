        while (1) {
                c = CIRC_SPACE_TO_END(cb->head, cb->tail, DBG_CIRC_BUF_SIZE);
                printk( KERN_INFO "Circ space to end: %d msg length: %d\n", c, count);

                if (count < c)
                        c = count;

                if (c <= 0)
                        break;
                
                if (CIRC_SPACE_TO_END(cb->head, cb->tail, DBG_CIRC_BUF_SIZE) <= 0)
                {
                  //free some space in circ buffer
                  cb->tail = (cb->tail + count) & (DBG_CIRC_BUF_SIZE-1);
                  c = CIRC_SPACE_TO_END(cb->head, cb->tail, DBG_CIRC_BUF_SIZE);
                  printk( KERN_INFO "New avaliable space: %d msg length: %d\n", c, count);
                  memcpy(cb->buf + cb->head, buf, count);
//                  printk( KERN_INFO "MSG: %s\n", buf);
//                  printk( KERN_INFO "Buf: %s\n", cb->buf+cb->head);
                  cb->head = (cb->head + count) & (DBG_CIRC_BUF_SIZE-1);
                  printk( KERN_INFO "Head: %d Tail: %d\n", cb->head, cb->tail);
                  break;
                }
                else
                {
                  memcpy(cb->buf + cb->head, buf, c);
                  cb->head = (cb->head + c) & (DBG_CIRC_BUF_SIZE-1);
                  printk( KERN_INFO "Head: %d Tail: %d\n", cb->head, cb->tail);
                }

                buf += c;
                count -= c;
                ret += c;
        }


