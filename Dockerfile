FROM ubuntu:latest

RUN apt-get update && apt-get install -y \
    crossbuild-essential-arm64 \
    qemu-system-arm \
    gcc-arm-linux-gnueabi \
    cpio \
    git \
    flex \
    bison \
    bc \
    mc

# Clone the Linux kernel source
RUN git clone --depth 1 https://github.com/torvalds/linux.git /linux

# Copy the Linux kernel source and your module into the image
COPY testmodule /linux/drivers/testmodule

# Copy your root filesystem cpio into the image
COPY rootfscpio /rootfscpio

# Copy the compile_and_run_qemu.sh script into the image
COPY compile_and_run_qemu.sh /linux/compile_and_run_qemu.sh

# Make the script executable
RUN chmod +x /linux/compile_and_run_qemu.sh

WORKDIR /linux
